import {ROUTES} from '../../router/Routes.enum'
import {PageFormLayout} from '../../layouts/PageFormLayout/PageFormLayout'
import {ButtonLink} from '../../components/Button/ButtonLink'
import React from 'react'

export const NotFoundPage: React.FC = (): JSX.Element => {
  return (
    <article className='page-not-found'>
      <PageFormLayout
        pageTitle='URL not found'
        controls={
          <ButtonLink label='return to root' url={ROUTES.root} />
        }
        body={
          <div>404 Page not found</div>
        }
      />
    </article>
  )
}
