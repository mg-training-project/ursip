import React, {useEffect, useState} from 'react'
import {ArticleList} from '../../components/ArticleList/ArticleList'
import {useDispatch} from 'react-redux'
import {useTypedSelector} from '../../hooks/useTypedSelector'
import {
  IActionArticleList,
  IArticleSetRangeDate,
  IArticleSort,
} from '../../store/article/IStateArticle.interface'
import {Dispatch} from 'redux'
import {actions} from '../../store/actions'
import {
  PageControlLayout,
} from '../../layouts/PageControlLayuot/PageControl.layout'
import {Button} from '../../components/Button/Button'
import {SORTING_DIRECTION} from '../../core/SortingDirection.enum'
import {ArticleService} from '../../domain/Article/Article.service'
import {DateRangePicker, RangeKeyDict} from 'react-date-range'
import {Modal} from '../../components/Modal/Modal'
import {DateTimeService} from '../../Infrastucture/DateTime/DateTime.service'
import {Pagination} from '../../components/Pagination/Pagination'
import {ROUTES} from '../../router/Routes.enum'
import {APP_CONFIG} from '../../app.config'
import {Loader} from '../../components/Loader/Loader'
import {ButtonLink} from '../../components/Button/ButtonLink'
import {IAppState} from '../../store/AppState.interface'
import {
  IActionLoading,
} from '../../store/infrastucture/IStateInfrastructure.interface'
import {useApi} from '../../api/useApi.hook'

export const ArticleListPage: React.FC = (): JSX.Element => {
  const {list, dateRange, currentPage, pageSize} = useTypedSelector(
    (state: IAppState) => state.article)
  const {loading} = useTypedSelector(
    (state: IAppState) => state.infrastructure)
  const dispatch: Dispatch<IActionArticleList |
    IArticleSort |
    IArticleSetRangeDate |
    IActionLoading> = useDispatch()
  const api = useApi()
  const [modalIsOpen, setModalIsOpen] = useState<boolean>(false)

  const [startDate, endDate] = dateRange
  const RANGE = 'RANGE'
  const selectionRange = {
    startDate,
    endDate,
    key: RANGE,
  }

  useEffect(() => {
    dispatch(actions.infrastructure.loading(true))
    // Серверо трабатывает быстро вставлена обертка чтобы видеть лоадер
    setTimeout(() => {
      api.getArticleList({
        page: currentPage,
        pageSize: pageSize,
      })
        .then(data => {
          if (data) {
            dispatch(actions.article.fetchArticleListActionSuccess(data))
          }
        })
        .finally(() => {
          dispatch(actions.infrastructure.loading(false))
        })
    }, APP_CONFIG.timeout)
  }, [dispatch, api, currentPage, pageSize])

  const sort = (direction: SORTING_DIRECTION) => {
    dispatch(actions.article.sortArticleByDate(direction))
  }

  const rangeSelect = (range: RangeKeyDict) => {
    dispatch(actions.article.setArticleRangeDate(
      [
        range[RANGE].startDate || startDate,
        range[RANGE].endDate
          ? DateTimeService.getEndDay(range[RANGE].endDate)
          : endDate,
      ],
    ))
    setModalIsOpen(false)
  }

  return (
    <article className="page-article-list">
      <PageControlLayout
        search={
          <ButtonLink label="Search" url={ROUTES.articleSearch}/>
        }
        new={
          <ButtonLink label='New Article' url={ROUTES.articleNew} />
        }
        pagination={
          <Pagination currentPage={currentPage} pageSize={pageSize}/>
        }
        calendar={
          modalIsOpen
            ?
            <Modal
              isOpen={modalIsOpen}
              close={() => setModalIsOpen(false)}
              body={
                <DateRangePicker
                  ranges={[selectionRange]}
                  onChange={rangeSelect}
                  moveRangeOnFirstSelection={false}

                />
              }
            />
            : <Button
              label="Open calendar"
              action={() => setModalIsOpen(true)}
            />
        }
        sort={
          <>
            <Button
              label="Sort ASC"
              action={() => sort(SORTING_DIRECTION.asc)}
            />
            <Button
              label="Sort DESC"
              action={() => sort(SORTING_DIRECTION.desc)}
            />
          </>
        }
      />
      <h1 className="article-list__title">Articles:</h1>
      {loading
        ? <Loader/>
        : <ArticleList
          articleList={ArticleService.filter(list, dateRange)}
        />
      }
    </article>
  )
}
