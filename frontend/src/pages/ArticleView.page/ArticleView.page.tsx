import {useParams} from 'react-router-dom'
import React, {useEffect} from 'react'
import {useTypedSelector} from '../../hooks/useTypedSelector'
import {ArticleView} from '../../components/ArticleView/ArticleView'
import {Dispatch} from 'redux'
import {
  IActionCurrentArticle,
} from '../../store/article/IStateArticle.interface'
import {useDispatch} from 'react-redux'
import {actions} from '../../store/actions'
import {CommentList} from '../../components/CommentList/CommentList'
import {IActionCommentList} from '../../store/comment/IStateComment.interface'
import {ButtonBack} from '../../components/Button/ButtonBack'
import {PageFormLayout} from '../../layouts/PageFormLayout/PageFormLayout'
import {Loader} from '../../components/Loader/Loader'
import {APP_CONFIG} from '../../app.config'
import {
  IActionLoading,
} from '../../store/infrastucture/IStateInfrastructure.interface'
import {useApi} from '../../api/useApi.hook'

export const ArticleViewPage: React.FC = (): JSX.Element => {
  const {id} = useParams<{ id?: string }>()
  const api = useApi()
  const dispatch: Dispatch<IActionCurrentArticle |
    IActionCommentList |
    IActionLoading> = useDispatch()
  const currentArticle = useTypedSelector(
    (state) => state.article.current)
  const loading = useTypedSelector(
    (state) => state.infrastructure.loading)

  useEffect(() => {
    if (id) {
      dispatch(actions.infrastructure.loading(true))
      // Сервер отрабатывает быстро вставлена обертка чтобы видеть лоадер
      setTimeout(() => {
        api.getArticleById(id)
          .then(data => {
            if (data) {
              dispatch(actions.article.fetchCurrentArticleActionSuccess(data))
            }
          })
          .finally(() => {
            dispatch(actions.infrastructure.loading(false))
          })
      }, APP_CONFIG.timeout)
    }
  }, [api, dispatch, id])

  return (
    <article className="page-article-view">
      <PageFormLayout
        controls={
          <ButtonBack/>
        }
        body={
          <>
            {!id
              ? <p className="page-article-view__no-id">
                {`Invalid Article id`}
              </p>
              : loading
                ? <Loader/>
                : currentArticle
                  ? <div>
                    <ArticleView article={currentArticle}/>
                    <div className="article-view__comments">
                      <h4>Comments:</h4>
                      <CommentList/>
                    </div>
                  </div>
                  : <p className="page-article-view__not-found">
                    {`Article with id:${id} not found`}
                  </p>
            }
          </>
        }/>
    </article>
  )
}
