import {PageFormLayout} from '../../layouts/PageFormLayout/PageFormLayout'
import {ButtonBack} from '../../components/Button/ButtonBack'
import React from 'react'
import {ArticleForm} from '../../components/ArticleForm/ArticleForm'

export const ArticleFormPage: React.FC = (): JSX.Element => {
  return (
    <article className="page-new-article">
      <PageFormLayout
        pageTitle="Create new Article"
        controls={
          <ButtonBack/>
        }
        body={
          <ArticleForm/>
        }
      />
    </article>
  )
}
