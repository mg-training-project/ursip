import {Search} from '../../components/Search/Search'
import React, {useEffect, useState} from 'react'
import {useTypedSelector} from '../../hooks/useTypedSelector'
import {ArticleService} from '../../domain/Article/Article.service'
import {ArticleList} from '../../components/ArticleList/ArticleList'
import {PageFormLayout} from '../../layouts/PageFormLayout/PageFormLayout'
import {ButtonBack} from '../../components/Button/ButtonBack'
import {Loader} from '../../components/Loader/Loader'
import {useApi} from '../../api/useApi.hook'
import {useDispatch} from 'react-redux'
import {actions} from '../../store/actions'
import {APP_CONFIG} from '../../app.config'

export const ArticleSearchPage: React.FC = (): JSX.Element => {
  const {list} = useTypedSelector((state) => state.article)
  const {loading} = useTypedSelector((state) => state.infrastructure)
  const api = useApi()
  const dispatch = useDispatch()
  const [searchString, setSearchString] = useState<string>('')

  const filteredArticleList = ArticleService.filterByTitle(list, searchString)

  useEffect(() => {
    dispatch(actions.infrastructure.loading(true))
    // Сервер отрабатывает быстро вставлена обертка чтобы видеть лоадер
    setTimeout(() => {
      api.getArticleAllList()
        .then((list) => {
          if (list) {
            dispatch(actions.article.fetchArticleListActionSuccess(list))
          }
        })
        .finally(() => {
          dispatch(actions.infrastructure.loading(false))
        })
    }, APP_CONFIG.timeout)
  }, [api, dispatch])

  return (
    <PageFormLayout
      pageTitle="Search article by title"
      controls={
        <ButtonBack/>
      }
      body={
        <article className="search-page">
          {loading
            ? <Loader/>
            : <>
              <div className="search-page__search">
                <Search
                  searchString={searchString}
                  input={setSearchString}
                  placeholder="Search by title"
                />
              </div>
              <div className="search-page__result">
                {!!searchString && (filteredArticleList.length
                  ? <ArticleList
                    articleList={filteredArticleList}
                  />
                  : <div className="search-page__result-no-data">
                    Nothing found
                  </div>)}
              </div>
            </>
          }
        </article>
      }
    />
  )
}
