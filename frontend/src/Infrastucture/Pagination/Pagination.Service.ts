import {LimitOffset, PaginationData} from './Pagination.types'

export class PaginationService {
  static getPagePageSizeByLimitOffset(data: LimitOffset): PaginationData {
    return {
      page: (data.offset + data.limit) / data.limit,
      pageSize: data.limit,
    }
  }

  static getLimitOffsetByPagePerPage(paginationData: PaginationData): LimitOffset {
    return {
      limit: paginationData.pageSize,
      offset: (paginationData.page * paginationData.pageSize) - paginationData.pageSize,
    }
  }
}
