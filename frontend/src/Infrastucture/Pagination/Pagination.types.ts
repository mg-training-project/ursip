export type Limit = number
export type Offset = number
export type Page = number
export type PageSize = number

export interface PaginationData {
  page: Page,
  pageSize: PageSize
}

export interface LimitOffset {
  limit: Limit
  offset: Offset
}
