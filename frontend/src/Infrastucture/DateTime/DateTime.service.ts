import {DAY_IN_MS} from './DAY_IN_MS.const'

export class DateTimeService {
  static getEndDay(date: Date): Date {
    return new Date(date.getTime() + DAY_IN_MS)
  }
}
