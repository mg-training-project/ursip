import {IDtoNewAppError} from './IDtoAppError.interface'
import {AppError} from './AppError'
import {v4 as uuidv4} from 'uuid'

export class AppErrorFactory {
  create(dto: IDtoNewAppError): AppError {
    return new AppError({
      ...dto,
      id: uuidv4(),
    })
  }

  createCollection(dtoList: IDtoNewAppError[]): AppError[] {
    return dtoList.map(dto => this.create(dto))
  }
}
