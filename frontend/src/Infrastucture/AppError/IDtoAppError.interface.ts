import {UUID} from '../../core/core.type'

export interface IDtoAppError {
  message: string
  id: UUID
  parent?: string
}

export interface IDtoNewAppError {
  message: string
  parent?: string
}
