import {UUID} from '../../core/core.type'
import {IDtoAppError} from './IDtoAppError.interface'

export class AppError {
  message: string
  id: UUID
  parent?: string

  constructor(dto: IDtoAppError) {
    this.id = dto.id
    this.message = dto.message
    this.parent = dto.parent
  }
}
