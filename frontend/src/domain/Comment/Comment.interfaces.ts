import {ArticleId} from '../Article/Article.intefaces'

export type CommentId = number

export interface IDtoComment {
  id: CommentId | null
  user: string
  text: string
  article: ArticleId
}
