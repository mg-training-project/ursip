import {IDtoComment} from './Comment.interfaces'

export const DEFAULT_DTO_COMMENT: IDtoComment = {
  id: null,
  user: '',
  text: '',
  article: '',
}
