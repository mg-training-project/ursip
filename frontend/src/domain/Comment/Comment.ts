import {ArticleId} from '../Article/Article.intefaces'
import {IDtoComment} from './Comment.interfaces'

export type CommentId = number

export class Comment {
  id: CommentId | null
  user?: string
  text: string
  article: ArticleId

  constructor(dto: IDtoComment) {
    this.id = dto.id
    this.user = dto.user
    this.text = dto.text
    this.article = dto.article
  }

  public getDto(): IDtoComment {
    return {
      id: this.id,
      user: this.user || '',
      text: this.text,
      article: this.article
    }
  }
}
