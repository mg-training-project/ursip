import {IDtoComment} from './Comment.interfaces'
import {Comment, CommentId} from './Comment'
import {ArticleId} from '../Article/Article.intefaces'

export class CommentFactory {
  create(dto: IDtoComment): Comment {
    return new Comment(dto)
  }

  createCollection(dtoList: IDtoComment[]): Comment[] {
    return dtoList.map(dto => this.create(dto))
  }

  createNew(articleId: ArticleId, text?: string, user?: string, id?: CommentId): Comment {
    return new Comment({
      article: articleId,
      text: text || '',
      user: user || 'Unknown user',
      id: id || new Date().getTime()
    })
  }
}
