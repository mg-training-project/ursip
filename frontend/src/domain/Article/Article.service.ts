import {Article} from './Article'
import {SORTING_DIRECTION} from '../../core/SortingDirection.enum'
import {IDateMaxMin} from './Article.intefaces'
import {DateTimeService} from '../../Infrastucture/DateTime/DateTime.service'

export class ArticleService {
  static sortByDate(
    articleList: Article[],
    direction: SORTING_DIRECTION = SORTING_DIRECTION.asc,
  ): Article[] {
    return articleList.sort((a: Article, b: Article) => {
      if (direction === SORTING_DIRECTION.asc) {
        return a.date.getTime() - b.date.getTime()
      }
      return b.date.getTime() - a.date.getTime()
    })
  }

  static filterByTitle(
    articleList: Article[],
    searchString: string,
  ): Article[] {
    return articleList.filter(article => {
      return article.title.includes(searchString)
    })
  }

  static filter(
    articleList: Article[],
    dateRange: IDateMaxMin,
  ): Article[] {
    const [startDate, endDate] = dateRange
    return articleList.filter(article => {
      return article.date.getTime() >= startDate.getTime()
        && article.date.getTime() <= endDate.getTime()
    })
  }

  static getMamMinArticleDate(articleList: Article[]): IDateMaxMin {
    return articleList.reduce(
      (acc: IDateMaxMin, article: Article, index: number): IDateMaxMin => {
        let [min, max] = acc
        if (!index) {
          min = article.date
          max = DateTimeService.getEndDay(article.date)
        }
        return [
          min.getTime() >= article.date.getTime()
            ? article.date
            : min,
          max.getTime() <= article.date.getTime()
            ? DateTimeService.getEndDay(article.date)
            : max,
        ]
      }, [new Date(), DateTimeService.getEndDay(new Date())])
  }
}
