import {IDtoArticle} from './Article.intefaces'
import {Article} from './Article'
import { v4 as uuidv4 } from 'uuid';

export class ArticleFactory {
  create(dto: IDtoArticle): Article {
    return new Article(dto)
  }

  createCollection(dtoList: IDtoArticle[]): Article[] {
    return dtoList.map(dto => this.create(dto))
  }

  createNew(title: string, text: string): Article {
    return new Article({
      id: uuidv4(),
      date: new Date().toISOString(),
      title: title,
      text: text
    })
  }
}
