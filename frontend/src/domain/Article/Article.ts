import {ArticleId, IDtoArticle} from './Article.intefaces'
import {DateTime} from '../../core/core.type'

export class Article {
  public readonly id: ArticleId
  private readonly dateOriginal: DateTime
  public readonly title: string
  public readonly text?: string
  public last: boolean = false

  constructor(dto: IDtoArticle) {
    this.id = dto.id || ''
    this.dateOriginal = dto.date || new Date().toISOString()
    this.title = dto.title
    this.text = dto.text
  }

  get date(): Date {
    return new Date(this.dateOriginal)
  }

  get dateString(): string {
    return `${this.date.getDate()}.${this.date.getMonth() + 1}.${this.date.getFullYear()}`
  }

  getDto(): IDtoArticle {
    return {
      id: this.id,
      date: this.date.toISOString(),
      title: this.title,
      text: this.text
    }
  }
}
