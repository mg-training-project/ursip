import {INewDtoArticle} from './Article.intefaces'

export const DEFAULT_DTO_ARTICLE: INewDtoArticle = {
  id: null,
  date: null,
  title: '',
  text: '',
}
