import {DateTime} from '../../core/core.type'

export type ArticleId = string

export type IDtoArticle = ISavedDtoArticle | INewDtoArticle

export interface ISavedDtoArticle {
  id: ArticleId,
  date: DateTime,
  title: string,
  text?: string
}

export interface INewDtoArticle {
  id: null,
  date: null,
  title: string,
  text: string
}

export type StartDate = Date
export type EndDate = Date
export type IDateMaxMin = [StartDate, EndDate]
