import {TypedUseSelectorHook, useSelector} from 'react-redux'
import {IAppState} from '../store/AppState.interface'

export const useTypedSelector: TypedUseSelectorHook<IAppState> = useSelector
