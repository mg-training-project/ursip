export enum SORTING_DIRECTION {
  asc = 'ASC',
  desc = 'DESC',
}
