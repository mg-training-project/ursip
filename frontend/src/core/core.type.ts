export type DateTime = string // example: '2020-06-09T15:03:23.000Z'
export type UUID = string
