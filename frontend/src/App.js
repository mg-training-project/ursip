import {AppRouter} from './router/App.router';
import {Provider} from 'react-redux';
import {store} from './store';
import React from 'react';
import {ApiProvider} from './api/api.provider';

function App() {
  return (
    <Provider store={store}>
      <ApiProvider>
        <AppRouter/>
      </ApiProvider>
    </Provider>
  );
}

export default App;
