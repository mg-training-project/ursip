import React, {FormEvent, useMemo, useState} from 'react'
import {Button} from '../Button/Button'
import './ArticleForm.scss'
import {ArticleFactory} from '../../domain/Article/Article.factory'
import {Dispatch} from 'redux'
import {useDispatch} from 'react-redux'
import {useApi} from '../../api/useApi.hook'
import {actions} from '../../store/actions'
import {
  IActionInfrastructure,
} from '../../store/infrastucture/IStateInfrastructure.interface'
import {APP_CONFIG} from '../../app.config'
import {ISetLastArticle} from '../../store/article/IStateArticle.interface'
import {useNavigate} from 'react-router-dom'
import {ROUTES} from '../../router/Routes.enum'
import {useTypedSelector} from '../../hooks/useTypedSelector'
import {IAppState} from '../../store/AppState.interface'
import {Loader} from '../Loader/Loader'

export const ArticleForm: React.FC = (): JSX.Element => {
  const articleFactory = useMemo(() => new ArticleFactory(), [])
  const api = useApi()
  const dispatch: Dispatch<IActionInfrastructure | ISetLastArticle> = useDispatch()
  const navigate = useNavigate()
  const {loading} = useTypedSelector(
    (state: IAppState) => state.infrastructure)

  const [articleTitle, setArticleTitle] = useState<string>('')
  const [articleText, setArticleText] = useState<string>('')

  const sendArticle = () => {

    const newArticle = articleFactory.createNew(articleTitle, articleText)
    dispatch(actions.infrastructure.loading(true))
    // Сервер отрабатывает быстро вставлена обертка чтобы видеть лоадер
    setTimeout(() => {
      api.postArticle(newArticle)
        .then((data) => {
          if (data) {
            dispatch(actions.article.setLastArticle(data))
            navigate(ROUTES.root)
          }
        })
        .finally(() => {
          dispatch(actions.infrastructure.loading(false))
        })
    }, APP_CONFIG.timeout)
  }

  return (
    loading
      ? <Loader/>
      : <div className="article-form">
        <input
          className="article-form__title"
          value={articleTitle}
          onInput={(event: FormEvent<HTMLInputElement>) => {
            setArticleTitle(event.currentTarget.value)
          }}
        />
        <textarea
          className="article-form__text"
          value={articleText}
          rows={10}
          onInput={(event: FormEvent<HTMLTextAreaElement>) => {
            setArticleText(event.currentTarget.value)
          }}
        />
        <div className="article-form__controls">

          <Button
            className="article-form__button-send"
            label="Send new Article"
            action={sendArticle}
            disabled={!articleTitle || !articleText}
          />
        </div>
      </div>
  )
}
