import {IPropsSearch} from './IPropsSearch.interface'
import React, {FormEvent} from 'react'
import './Search.scss'

export const Search: React.FC<IPropsSearch> = (props: IPropsSearch): JSX.Element => {
  const input = (event: FormEvent<HTMLInputElement>) => {
    props.input(event.currentTarget.value)
  }

  return (
    <input
      className='search'
      type='search'
      value={props.searchString}
      onInput={(event: FormEvent<HTMLInputElement>) => input(event)}
      placeholder={props.placeholder || ''}
    />
  )
}
