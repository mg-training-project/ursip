export interface IPropsSearch {
  searchString: string
  input(searchSubstring: string): void
  placeholder?: string
}
