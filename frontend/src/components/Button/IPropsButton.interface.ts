import {ROUTES} from '../../router/Routes.enum'

export interface IPropsButton {
  label: string
  action(): void
  disabled?: boolean
  className?: string
}

export interface IPropsButtonLink {
  label: string
  url: ROUTES
  disabled?: boolean
}

export interface IPropsButtonBack {
  disabled?: boolean
}
