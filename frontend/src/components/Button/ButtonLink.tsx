import {IPropsButtonLink} from './IPropsButton.interface'
import {Link} from 'react-router-dom'
import React from 'react'

export const ButtonLink: React.FC<IPropsButtonLink> = (props: IPropsButtonLink): JSX.Element => {
  return (
    <Link
      to={props.url}
      className={[
        'button',
        'button-back',
        props.disabled
          ? 'button--disabled'
          : '',
      ].join(' ')}
    >
      {props.label}
    </Link>
  )
}
