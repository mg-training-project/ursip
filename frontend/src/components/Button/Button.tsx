import {IPropsButton} from './IPropsButton.interface'
import './Button.scss'
import React from 'react'

export const Button: React.FC<IPropsButton> = (props: IPropsButton): JSX.Element => {
  return (
    <button
      className={[
        'button',
        props.className || '',
        props.disabled
          ? 'button--disabled'
          : ''].join(' ')}
      onClick={() => props.action()}
      disabled={props.disabled}
    >
      {props.label}
    </button>
  )
}
