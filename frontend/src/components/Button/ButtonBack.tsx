import {ROUTES} from '../../router/Routes.enum'
import {Link} from 'react-router-dom'
import './Button.scss'
import {IPropsButtonBack} from './IPropsButton.interface'
import React from 'react'

export const ButtonBack: React.FC<IPropsButtonBack> = (props: IPropsButtonBack): JSX.Element => {
  return (
    <Link
      to={ROUTES.root}
      className={[
        'button',
        'button-back',
        props.disabled
          ? 'button--disabled'
          : '',
      ].join(' ')}
    >
      {'< Back'}
    </Link>
  )
}
