import {IPropsArticleView} from './IPropsArticleView.interface'
import './ArticleView.scss'
import React from 'react'

export const ArticleView: React.FC<IPropsArticleView> = (props: IPropsArticleView): JSX.Element => {
  return (
    <article className="article-view">
      <div className="article-view__body">
        <h2 className="article-view__title">{props.article.title}</h2>
        <p className="article-view__text">{props.article.text}</p>
      </div>
    </article>
  )
}
