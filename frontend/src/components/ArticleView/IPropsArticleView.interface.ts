import {Article} from '../../domain/Article/Article'

export interface IPropsArticleView {
  article: Article
}
