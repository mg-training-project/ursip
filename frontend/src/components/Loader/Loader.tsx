import loader from './loader.gif'
import './Loader.scss'
import React from 'react'

export const Loader: React.FC = (): JSX.Element => {
  return (
    <div className="loader">
      <img alt="loader" src={loader}/>
    </div>
  )
}
