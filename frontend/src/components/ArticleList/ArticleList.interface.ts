import {Article} from '../../domain/Article/Article'

export interface IPropsArticleList {
  articleList: Article[]
}
