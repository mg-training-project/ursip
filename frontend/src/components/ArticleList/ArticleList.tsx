import {IPropsArticleList} from './ArticleList.interface'
import {Link} from 'react-router-dom'
import {ROUTES} from '../../router/Routes.enum'
import './ArticleList.scss'
import {Article} from '../../domain/Article/Article'
import {actions} from '../../store/actions'
import {
  ArticleListItemLayout,
} from '../../layouts/ArticleListItemLayout/ArticleListItem.layout'
import {Button} from '../Button/Button'
import {useDispatch} from 'react-redux'
import React from 'react'

export const ArticleList: React.FC<IPropsArticleList> = (props: IPropsArticleList): JSX.Element => {
  const dispatch = useDispatch()

  const deleteArticle = (article: Article) => {
    dispatch(actions.article.removeArticleById(article.id))
  }

  return (
    <>

      <div className="article-list">
        {!!props.articleList.length
          ? props.articleList.map(article => (
            <div
              className={['article', article.last ? 'article--last' : ''].join(' ')}
              key={article.id}
            >
              <ArticleListItemLayout
                title={<h3 className="article__title">
                  <Link
                    to={`${ROUTES.article}/${article.id}`}
                  >{article.title}</Link>
                </h3>}
                data={
                  <span className="article__date">{article.dateString}</span>
                }
                controls={
                  <Button label="Delete" action={() => deleteArticle(article)}/>
                }
              />
            </div>
          ))
          : <div className="article-list__no-data">{'< no Articles >'}</div>}
      </div>
    </>
  )
}
