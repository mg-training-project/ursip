import {Page, PageSize} from '../../Infrastucture/Pagination/Pagination.types'

export interface IPropsPagination {
  currentPage: Page
  pageSize: PageSize
}
