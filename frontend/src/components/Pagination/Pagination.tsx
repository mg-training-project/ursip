import {Button} from '../Button/Button'
import React, {FormEvent} from 'react'
import {IPropsPagination} from './IPropsPagination.interface'
import './Pagination.scss'
import {useDispatch} from 'react-redux'
import {
  IPaginationSetPage,
  IPaginationSetPageSize,
} from '../../store/article/IStateArticle.interface'
import {Dispatch} from 'redux'
import {actions} from '../../store/actions'
import {Page} from '../../Infrastucture/Pagination/Pagination.types'

export const Pagination: React.FC<IPropsPagination> = (props: IPropsPagination): JSX.Element => {

  const dispatch = useDispatch<Dispatch<IPaginationSetPage | IPaginationSetPageSize>>()

  const setPage = (pageModifier: Page) => {
    dispatch(
      actions.article.setPaginationPage(props.currentPage + pageModifier))
  }

  const setPageSize = (event: FormEvent<HTMLInputElement>) => {

    dispatch(
      actions.article.setPaginationPageSize(Number(event.currentTarget.value)))
  }

  return (
    <div className="pagination">
      <Button
        label="<"
        action={() => setPage(-1)}
        disabled={props.currentPage <= 1}
      />
      <div className="pagination__current-page">{props.currentPage}</div>
      <span className="pagination__per-page-label">page size:</span>
      <input
        className="pagination__per-page"
        type="number"
        value={props.pageSize}
        onInput={setPageSize}
      />
      <Button
        label=">"
        action={() => setPage(1)}/>
    </div>
  )
}
