import React from 'react'
import {useTypedSelector} from '../../hooks/useTypedSelector'
import {AppError} from '../../Infrastucture/AppError/AppError'
import {Button} from '../Button/Button'
import {UUID} from '../../core/core.type'
import {useDispatch} from 'react-redux'
import {
  IActionInfrastructure,
} from '../../store/infrastucture/IStateInfrastructure.interface'
import {Dispatch} from 'redux'
import {actions} from '../../store/actions'
import './ErrorBar.scss'

export const ErrorBar: React.FC = (): JSX.Element => {
  const errorList = useTypedSelector(
    (state) => state.infrastructure.errorList)
  const dispatch = useDispatch<Dispatch<IActionInfrastructure>>()

  const removeError = (id: UUID) => {
    dispatch(actions.infrastructure.removeError(id))
  }

  return (
    <article className="error-bar">
      {errorList.map((error: AppError) => (
        <div className="error-bar__error">
          <div className="error-bar__error-body">
            <span className="error-bar__error-parent">
              {error.parent}
            </span>
            <span className="error-bar__error-message">
              {error.message}
            </span>
          </div>
          <Button className="error-bar__error-button-remove" label="X"
                  action={() => removeError(error.id)}/>
        </div>
      ))}
    </article>
  )
}
