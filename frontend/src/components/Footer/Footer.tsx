import './Footer.scss'
import React from 'react'

export const Footer: React.FC = (): JSX.Element => {

  const year = new Date().getFullYear()

  return (
    <footer className="app-footer">
      <div className="app-footer__body">
        <span className="app-footer__year">{year}</span>
      </div>
    </footer>
  )
}
