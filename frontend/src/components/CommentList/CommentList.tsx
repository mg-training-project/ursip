import {useTypedSelector} from '../../hooks/useTypedSelector'
import './CommentList.scss'
import {Button} from '../Button/Button'
import React, {
  FormEvent,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react'
import {Dispatch} from 'redux'
import {
  IActionCurrentArticle,
} from '../../store/article/IStateArticle.interface'
import {IActionCommentList} from '../../store/comment/IStateComment.interface'
import {useDispatch} from 'react-redux'
import {actions} from '../../store/actions'
import {CommentFactory} from '../../domain/Comment/Comment.factory'
import {Loader} from '../Loader/Loader'
import {APP_CONFIG} from '../../app.config'
import {
  IActionLoading,
} from '../../store/infrastucture/IStateInfrastructure.interface'
import {useApi} from '../../api/useApi.hook'

export const CommentList: React.FC = (): JSX.Element => {
  const {list} = useTypedSelector((state) => state.comment)
  const [localLoading, setLocalLoading] = useState<boolean>(false)
  const errorList = useTypedSelector((state) => state.infrastructure.errorList)

  const [showComment, setShowComment] = useState<boolean>(false)
  const [newCommentText, setNewCommentText] = useState<string>('')
  const commentFactory = useMemo<CommentFactory>(() => new CommentFactory(), [])

  const api = useApi()
  const dispatch: Dispatch<IActionCurrentArticle
    | IActionCommentList
    | IActionLoading> = useDispatch()
  const currentArticle = useTypedSelector(
    (state) => state.article.current)

  const getCommentData = useCallback(() => {
    if (currentArticle) {
      setLocalLoading(true)
      // Сервер отрабатывает быстро вставлена обертка чтобы видеть лоадер
      setTimeout(() => {
        api.getCommentByArticleId(currentArticle.id)
          .then(data => {
            if (data) {
              dispatch(actions.comment.fetchCommentListSuccess(data))
            }
          })
          .finally(() => {
            setLocalLoading(false)
          })
      }, APP_CONFIG.timeout)
    }
  }, [currentArticle, dispatch, api])

  useEffect(() => {
    if (showComment) {
      getCommentData()
    }
  }, [showComment, getCommentData])

  const postNewComment = () => {
    if (currentArticle) {
      api.postComment(
        commentFactory.createNew(currentArticle.id, newCommentText),
      )
        .then(() => {
          setNewCommentText('')
        })
        .then(() => {
          getCommentData()
        })
    }
  }

  return (
    <div className="comment-list">
      <div className="comment-list__controls">
        <Button
          label={showComment
            ? 'hidde comment'
            : 'show comment'}
          action={() => setShowComment(!showComment)}
        />
      </div>
      {showComment &&
        <div className="comment-list__list">
          {localLoading
            ? <Loader/>
            : errorList.length
              ? <div
                className="comment-list__error">{errorList[0].message}</div>
              : list.length
                ? list.map(comment => (
                  <div
                    className="comment"
                    key={comment.id}
                  >
                    <div className="comment__user">{comment.user}</div>
                    <div className="comment__body">{comment.text}</div>
                  </div>
                ))
                : <div className="comment__no-comment">{'< no comments >'}</div>
          }
          <div className="comment-list__new-comment">
        <textarea
          className="comment-list__new-comment-text"
          placeholder="Please write comment"
          rows={5}
          value={newCommentText}
          onInput={(event: FormEvent<HTMLTextAreaElement>) => {
            setNewCommentText(event.currentTarget.value)
          }}
        />
            <div className="comment-list__controls">
              <Button
                label="Add new comment" action={postNewComment}
              />
            </div>
          </div>
        </div>
      }
    </div>
  )
}
