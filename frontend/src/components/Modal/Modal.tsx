import React from 'react'
import {IPropsModal} from './IPropsModal.interface'
import {CUSTOM_STYLES} from './ModalStyle.const'
import ReactModal from 'react-modal'

export const Modal: React.FC<IPropsModal> = (props: IPropsModal): JSX.Element => {

  return (
    <ReactModal
      isOpen={props.isOpen}
      onRequestClose={() => props.close()}
      style={CUSTOM_STYLES}
      contentLabel={props.title || 'Modal window'}
      ariaHideApp={false}
    >
      {props.body}
    </ReactModal>
  )
}
