export interface IPropsModal {
  body: JSX.Element
  isOpen: boolean
  title?: string
  close(): void
}
