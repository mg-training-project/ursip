import './Header.scss'
import logo from './logo.svg'
import React from 'react'
import {Link} from 'react-router-dom'
import {ROUTES} from '../../router/Routes.enum'

export const Header: React.FC = (): JSX.Element => {
  return (
    <header className="app-header">
      <div className="app-header__body">
        <Link to={ROUTES.root} >
          <img src={logo} alt="logo" width="60" height="60"/>
        </Link>
        <span className="app-header__title">Training project</span>
      </div>
    </header>
  )
}
