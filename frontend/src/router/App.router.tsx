import {Route, Routes} from 'react-router-dom'
import {ROUTES} from './Routes.enum'
import {ArticleListPage} from '../pages/ArticleList.page/ArticleList.page'
import {ArticleFormPage} from '../pages/ArticleForm.page/ArticleForm.page'
import {AppLayout} from '../layouts/AppLayout/App.layout'
import {ArticleViewPage} from '../pages/ArticleView.page/ArticleView.page'
import {ArticleSearchPage} from '../pages/ArticleSearch.page/ArticleSearch.page'
import {NotFoundPage} from '../pages/NotFound.page/NotFound.page'
import React from 'react'

export const AppRouter: React.FC = (): JSX.Element => {
  return (
    <Routes>
      <Route path={ROUTES.root} element={<AppLayout/>}>
        <Route index element={<ArticleListPage/>}/>
        <Route path={ROUTES.articleList} element={<ArticleListPage/>}/>
        <Route path={`${ROUTES.article}/:id`} element={<ArticleViewPage/>}/>
        <Route path={ROUTES.articleNew} element={<ArticleFormPage/>}/>
        <Route path={ROUTES.articleSearch} element={<ArticleSearchPage/>}/>
        <Route path={ROUTES.notFound} element={<NotFoundPage/>}/>
      </Route>
    </Routes>
  )
}

