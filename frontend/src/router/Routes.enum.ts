export enum ROUTES {
  root = '/',
  articleList = 'articles',
  article = 'article',
  articleSearch = 'article/search',
  articleNew = 'article/new',
  notFound = '*'
}
