import {IDtoArticle} from '../domain/Article/Article.intefaces'
import {IDtoComment} from '../domain/Comment/Comment.interfaces'

export interface IResponseArticleList {
  data: IDtoArticle[]
  total: number
}

export interface IResponseCommentList {
  records: IDtoComment[]
  total: number
}
