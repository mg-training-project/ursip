import {API_CONTEXT} from './api.context'
import React from 'react'
import {IPropsApiProvider} from './IPropsApiProvider.interface'
import {ApiService} from './ApiService'
import {useDispatch} from 'react-redux'

export const ApiProvider: React.FC<IPropsApiProvider> = (props: IPropsApiProvider): JSX.Element => {
  const dispatch = useDispatch()
  return (
    <API_CONTEXT.Provider value={{
      api: new ApiService(dispatch)
    }} >
      {props.children}
    </API_CONTEXT.Provider>
  )
}
