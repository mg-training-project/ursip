import React from 'react'
import {ApiService} from './ApiService'

export const API_CONTEXT = React.createContext({
  api: new ApiService()
})
