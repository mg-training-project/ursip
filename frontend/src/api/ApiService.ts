import axios, {AxiosError, AxiosResponse} from 'axios'
import {ENDPOINTS} from './endpoints.const'
import {IResponseArticleList, IResponseCommentList} from './api.interfaces'
import {ArticleId, IDtoArticle} from '../domain/Article/Article.intefaces'
import {IDtoComment} from '../domain/Comment/Comment.interfaces'
import {PaginationService} from '../Infrastucture/Pagination/Pagination.Service'
import {PaginationData} from '../Infrastucture/Pagination/Pagination.types'
import {Comment} from '../domain/Comment/Comment'
import {Article} from '../domain/Article/Article'
import {Dispatch} from 'redux'
import {
  IActionInfrastructure,
} from '../store/infrastucture/IStateInfrastructure.interface'
import {actions} from '../store/actions'

export class ApiService {
  private readonly dispatch: Dispatch<IActionInfrastructure> | null = null

  constructor(dispatch?: Dispatch<IActionInfrastructure>) {
    if (dispatch) {
      this.dispatch = dispatch
    }
  }

  getArticleList(paginationData: PaginationData): Promise<IDtoArticle[] | void> {
    const {limit, offset} = PaginationService
      .getLimitOffsetByPagePerPage(paginationData)

    return axios.get(`${ENDPOINTS.article}?limit=${limit}&offset=${offset}`)
      .then((response: AxiosResponse<IResponseArticleList>) => {
        return response.data.data
      })
      .catch((error: AxiosError) => {
        if (this.dispatch) {
          this.dispatch(actions.infrastructure.setError({
            message: error.message,
            parent: 'ApiService.getArticleList'
          }))
        }
      })
  }

  getArticleAllList(): Promise<IDtoArticle[] | void> {
    return axios.get(`${ENDPOINTS.article}`)
      .then((response: AxiosResponse<IResponseArticleList>) => {
        return response.data.data
      })
      .catch((error: AxiosError) => {
        if (this.dispatch) {
          this.dispatch(actions.infrastructure.setError({
            message: error.message,
            parent: 'ApiService.getArticleAllList'
          }))
        }
      })
  }

  getArticleById(id: ArticleId): Promise<IDtoArticle | void> {
    return axios.get(`${ENDPOINTS.article}/${id}`)
      .then((response: AxiosResponse<IDtoArticle>) => {
        return response.data
      })
      .catch((error: AxiosError) => {
        if (this.dispatch) {
          this.dispatch(actions.infrastructure.setError({
            message: error.message,
            parent: 'ApiService.getArticleById'
          }))
        }
      })
  }

  getCommentByArticleId(id: ArticleId): Promise<IDtoComment[] | void> {
    return axios.get(`${ENDPOINTS.comment}?article=${id}`)
      .then((response: AxiosResponse<IResponseCommentList>) => {
        return response.data.records
      })
      .catch((error: AxiosError) => {
        if (this.dispatch) {
          this.dispatch(actions.infrastructure.setError({
            message: error.message,
            parent: 'ApiService.getCommentByArticleId'
          }))
        }
      })
  }

  async postComment(comment: Comment): Promise<IDtoComment | void> {
    return axios.post(ENDPOINTS.comment, comment.getDto())
      .then((response: AxiosResponse<IDtoComment>) => {
        return response.data
      })
      .catch((error: AxiosError) => {
        if (this.dispatch) {
          this.dispatch(actions.infrastructure.setError({
            message: error.message,
            parent: 'ApiService.postComment'
          }))
        }
      })
  }

  async postArticle(article: Article): Promise<IDtoArticle | void> {
    return axios.post(ENDPOINTS.article, {...article.getDto()})
      .then((response: AxiosResponse<IDtoArticle>) => {
        return response.data
      })
      .catch((error: AxiosError) => {
        if (this.dispatch) {
          this.dispatch(actions.infrastructure.setError({
            message: error.message,
            parent: 'ApiService.postArticle'
          }))
        }
      })
  }
}
