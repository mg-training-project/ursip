import {useContext} from 'react'
import {API_CONTEXT} from './api.context'

export const useApi = () => {
  const {api} = useContext(API_CONTEXT)
  return api
}
