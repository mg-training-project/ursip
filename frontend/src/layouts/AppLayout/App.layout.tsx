import {Outlet} from 'react-router-dom'
import {Header} from '../../components/Header/Header'
import {Footer} from '../../components/Footer/Footer'
import './App.layout.scss'
import React from 'react'
import {ErrorBar} from '../../components/ErrorBar/ErrorBar'

export const AppLayout: React.FC = (): JSX.Element => {
  return (
    <div className="app-layout">
      <Header/>
      <div className="app-layout__error-bar">
        <ErrorBar />
      </div>
      <main className="app-layout__main">
        <Outlet/>
      </main>
      <Footer/>
    </div>

  )
}
