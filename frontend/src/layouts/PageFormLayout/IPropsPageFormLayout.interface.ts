export interface IPropsPageFormLayout {
  controls?: JSX.Element
  pageTitle?: string
  body: JSX.Element
}
