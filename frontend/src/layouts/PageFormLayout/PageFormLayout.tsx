import {IPropsPageFormLayout} from './IPropsPageFormLayout.interface'
import './PageFormLayout.scss'
import React from 'react'

export const PageFormLayout: React.FC<IPropsPageFormLayout> = (
  props: IPropsPageFormLayout
): JSX.Element => {
  return (
    <section className="page-form-layout">
      <div className="page-form-layout__header">
        <div className="page-form-layout__header-left">
          {!!props.controls &&
            <div className="page-form-layout__controls">
              {props.controls}
            </div>
          }
        </div>
        <div className="page-form-layout__header-center">
          {!!props.pageTitle &&
            <h2 className="page-form-layout__title">
              {props.pageTitle}
            </h2>
          }
        </div>
        <div className="page-form-layout__header-right"></div>
      </div>
      <div className="page-form-layout__main">
        {props.body}
      </div>
    </section>
  )
}
