import {
  IPropsArticleListItemLayout,
} from './IPropsArticleListItemLayout.interface'
import './ArticleListItemLayout.scss'
import React from 'react'

export const ArticleListItemLayout: React.FC<IPropsArticleListItemLayout> = (
  props: IPropsArticleListItemLayout,
): JSX.Element => {
  return (
    <div className="article-item">
      <div className="article-item__title">
        {props.title}
      </div>
      <div className="article-item__controls">
        {props.controls}
      </div>
      <div className="article-item__date">
        {props.data}
      </div>
    </div>
  )
}
