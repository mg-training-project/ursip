export interface IPropsArticleListItemLayout {
  title: JSX.Element
  data?: JSX.Element
  controls?: JSX.Element
}
