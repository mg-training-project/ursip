export interface IPropsPageControlLayout {
  search?: JSX.Element
  new?: JSX.Element
  calendar?: JSX.Element
  sort?: JSX.Element
  pagination?: JSX.Element
}
