import {IPropsPageControlLayout} from './IPropsPageControlLayout.interface'
import './PageControlLayout.scss'
import React from 'react'

export const PageControlLayout: React.FC<IPropsPageControlLayout> = (
  props: IPropsPageControlLayout,
): JSX.Element => {
  return (
    <div className="page-control-layout">
      <div className="page-control-layout__links">
        {props.search}
        {props.new}
      </div>
      <div className="page-control-layout__pagination">
        {props.pagination}
      </div>
      <div className="page-control-layout__sort">
        {props.calendar}
        {props.sort}
      </div>
    </div>
  )
}
