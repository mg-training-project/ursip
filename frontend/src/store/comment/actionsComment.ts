import {ACTION_TYPE} from '../ActionType.enum'
import {IDtoComment} from '../../domain/Comment/Comment.interfaces'
import {IActionFetchCommentListSuccess} from './IStateComment.interface'

export const actionsComment = {
  fetchCommentListSuccess: (payload: IDtoComment[]): IActionFetchCommentListSuccess => ({
    type: ACTION_TYPE.fetchCommentListSuccess,
    payload
  }),
}
