import {Comment} from '../../domain/Comment/Comment'
import {Action} from 'redux'
import {ACTION_TYPE} from '../ActionType.enum'
import {IDtoComment} from '../../domain/Comment/Comment.interfaces'

export interface IStateComment {
  list: Comment[],
}

export interface IActionFetchCommentListSuccess extends Action {
  type: ACTION_TYPE.fetchCommentListSuccess
  payload: IDtoComment[]
}

export type IActionCommentList = IActionFetchCommentListSuccess
