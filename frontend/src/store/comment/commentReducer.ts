import {DEFAULT_STATE_COMMENT} from './defaultStateComment.const'
import {IActionCommentList, IStateComment} from './IStateComment.interface'
import {ACTION_TYPE} from '../ActionType.enum'
import {CommentFactory} from '../../domain/Comment/Comment.factory'

export const commentReducer = (state: IStateComment = DEFAULT_STATE_COMMENT, action: IActionCommentList) => {
  const commentFactory = new CommentFactory()

  switch(action.type) {
    case ACTION_TYPE.fetchCommentListSuccess:
      return {
        ...state,
        loading: false,
        error: null,
        list: commentFactory.createCollection(action.payload)
      }
    default: return state
  }
}
