import {ACTION_TYPE} from '../ActionType.enum'
import {
  IActionFetchArticleListSuccess,
  IActionFetchCurrentArticleSuccess,
  IArticleRemove,
  IArticleSetRangeDate,
  IArticleSort,
  IPaginationSetPage,
  IPaginationSetPageSize, ISetLastArticle,
} from './IStateArticle.interface'
import {
  ArticleId,
  IDateMaxMin,
  IDtoArticle,
} from '../../domain/Article/Article.intefaces'
import {SORTING_DIRECTION} from '../../core/SortingDirection.enum'
import {Page, PageSize} from '../../Infrastucture/Pagination/Pagination.types'

export const actionsArticle = {
  fetchArticleListActionSuccess: (payload: IDtoArticle[]): IActionFetchArticleListSuccess => ({
    type: ACTION_TYPE.fetchArticleListSuccess,
    payload,
  }),
  fetchCurrentArticleActionSuccess: (payload: IDtoArticle): IActionFetchCurrentArticleSuccess => ({
    type: ACTION_TYPE.fetchCurrentArticleSuccess,
    payload,
  }),
  setLastArticle: (payload: IDtoArticle): ISetLastArticle => ({
    type: ACTION_TYPE.setLastArticle,
    payload,
  }),
  removeArticleById: (payload: ArticleId): IArticleRemove => ({
    type: ACTION_TYPE.removeArticle,
    payload,
  }),
  sortArticleByDate: (payload: SORTING_DIRECTION): IArticleSort => ({
    type: ACTION_TYPE.sortArticle,
    payload,
  }),
  setArticleRangeDate: (payload: IDateMaxMin): IArticleSetRangeDate => ({
    type: ACTION_TYPE.setRageDate,
    payload,
  }),
  setPaginationPage: (payload: Page): IPaginationSetPage => ({
    type: ACTION_TYPE.setCurrentPage,
    payload,
  }),
  setPaginationPageSize: (payload: PageSize): IPaginationSetPageSize => ({
    type: ACTION_TYPE.setPageSize,
    payload,
  }),
}
