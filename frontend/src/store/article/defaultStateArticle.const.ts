import {IStateArticle} from './IStateArticle.interface'

export const DEFAULT_STATE_ARTICLE: IStateArticle = {
  list: [],
  current: null,
  last: null,
  searchString: '',
  dateRange: [new Date(), new Date()],
  currentPage: 1,
  pageSize: 2,
}
