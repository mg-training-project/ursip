import {Article} from '../../domain/Article/Article'
import {IPageListState} from '../AppState.interface'
import {ACTION_TYPE} from '../ActionType.enum'
import {Action} from 'redux'
import {
  ArticleId,
  IDateMaxMin,
  IDtoArticle,
} from '../../domain/Article/Article.intefaces'
import {SORTING_DIRECTION} from '../../core/SortingDirection.enum'
import {Page, PageSize} from '../../Infrastucture/Pagination/Pagination.types'

export interface IStateArticle extends IPageListState {
  list: Article[]
  current: Article | null
  last: Article | null
  dateRange: IDateMaxMin
}

export interface IActionFetchArticleListSuccess extends Action {
  type: ACTION_TYPE.fetchArticleListSuccess
  payload: IDtoArticle[]
}

export type IActionArticleList = IActionFetchArticleListSuccess

export interface IActionFetchCurrentArticleSuccess extends Action {
  type: ACTION_TYPE.fetchCurrentArticleSuccess
  payload: IDtoArticle
}

export type IActionCurrentArticle = IActionFetchCurrentArticleSuccess

export interface IArticleRemove extends Action {
  type: ACTION_TYPE.removeArticle
  payload: ArticleId
}

export interface IArticleSort extends Action {
  type: ACTION_TYPE.sortArticle
  payload?: SORTING_DIRECTION
}

export interface IArticleSetRangeDate extends Action {
  type: ACTION_TYPE.setRageDate
  payload: IDateMaxMin
}

export interface IPaginationSetPage extends Action {
  type: ACTION_TYPE.setCurrentPage
  payload: Page
}

export interface IPaginationSetPageSize extends Action {
  type: ACTION_TYPE.setPageSize
  payload: PageSize
}

export interface ISetLastArticle extends Action {
  type: ACTION_TYPE.setLastArticle
  payload: IDtoArticle
}
