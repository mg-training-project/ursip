import {
  IActionArticleList,
  IActionCurrentArticle,
  IArticleRemove,
  IArticleSetRangeDate,
  IArticleSort, IPaginationSetPage, IPaginationSetPageSize, ISetLastArticle,
  IStateArticle,
} from './IStateArticle.interface'
import {DEFAULT_STATE_ARTICLE} from './defaultStateArticle.const'
import {ACTION_TYPE} from '../ActionType.enum'
import {ArticleFactory} from '../../domain/Article/Article.factory'
import {ArticleService} from '../../domain/Article/Article.service'

export const articleReducer = (
  state: IStateArticle = DEFAULT_STATE_ARTICLE,
  action:
    IActionArticleList |
    IActionCurrentArticle |
    IArticleRemove |
    IArticleSort |
    IArticleSetRangeDate |
    IPaginationSetPage |
    IPaginationSetPageSize |
    ISetLastArticle,
): IStateArticle => {
  const articleFactory = new ArticleFactory()

  switch(action.type) {
    case ACTION_TYPE.fetchArticleListSuccess:
      const list = articleFactory.createCollection(action.payload)
      if (state.last) {
        list.unshift(state.last)
      }
      return {
        ...state,
        list: [...list],
        last: null,
        dateRange: ArticleService.getMamMinArticleDate(list),
      }
    case ACTION_TYPE.fetchCurrentArticleSuccess:
      return {
        ...state,
        current: articleFactory.create(action.payload),
      }
    case ACTION_TYPE.setLastArticle:
      const article = articleFactory.create(action.payload)
      article.last = true
      return {
        ...state,
        last: article,
      }
    case ACTION_TYPE.removeArticle:
      return {
        ...state,
        list: state.list.filter(article => article.id !== action.payload),
      }
    case ACTION_TYPE.sortArticle:
      return {
        ...state,
        list: ArticleService.sortByDate(state.list, action.payload),
      }
    case ACTION_TYPE.setRageDate:
      return {
        ...state,
        dateRange: action.payload,
      }
    case ACTION_TYPE.setCurrentPage:
      return {
        ...state,
        currentPage: action.payload,
      }
    case ACTION_TYPE.setPageSize:
      return {
        ...state,
        pageSize: action.payload,
      }
    default:
      return state
  }
}
