export enum ACTION_TYPE {
  /** article */
  fetchArticleListSuccess    = 'FETCH_ARTICLE_LIST_SUCCESS',

  removeArticle              = 'REMOVE_ARTICLE',
  sortArticle                = 'SORT_ARTICLE',
  setRageDate                = 'SET_RAGE_DATE',

  setCurrentPage             = 'SET_CURRENT_PAGE',
  setPageSize                = 'SET_PAGE_SIZE',
  setLastArticle             = 'SET_LAST_ARTICLE',

  fetchCurrentArticleSuccess = 'FETCH_CURRENT_ARTICLE_SUCCESS',

  /** comment */
  fetchCommentListSuccess    = 'FETCH_COMMENT_LIST_SUCCESS',

  /** Infrastructure */
  loading                    = 'LOADING',
  setError                   = 'SET_ERROR',
  removeError                = 'REMOVE_ERROR',

}
