import {IStateInfrastructure} from './IStateInfrastructure.interface'

export const DEFAULT_STATE_INFRASTRUCTURE: IStateInfrastructure = {
  loading: false,
  errorList: []
}
