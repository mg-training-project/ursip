import {ACTION_TYPE} from '../ActionType.enum'
import {AppError} from '../../Infrastucture/AppError/AppError'
import {
  IDtoNewAppError,
} from '../../Infrastucture/AppError/IDtoAppError.interface'
import {UUID} from '../../core/core.type'

export interface IStateInfrastructure {
  loading: boolean
  errorList: AppError[]
}

export interface IActionLoading {
  type: ACTION_TYPE.loading
  payload: boolean
}

export interface IActionSetError {
  type: ACTION_TYPE.setError
  payload: IDtoNewAppError
}

export interface IActionRemoveError {
  type: ACTION_TYPE.removeError
  payload: UUID
}

export type IActionInfrastructure =
  IActionLoading |
  IActionSetError |
  IActionRemoveError
