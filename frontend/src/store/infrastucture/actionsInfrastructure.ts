import {
  IActionLoading,
  IActionRemoveError,
  IActionSetError,
} from './IStateInfrastructure.interface'
import {ACTION_TYPE} from '../ActionType.enum'
import {UUID} from '../../core/core.type'
import {
  IDtoNewAppError
} from '../../Infrastucture/AppError/IDtoAppError.interface'

export const actionsInfrastructure = {
  loading: (payload: boolean): IActionLoading => ({
    type: ACTION_TYPE.loading,
    payload
  }),
  setError: (payload: IDtoNewAppError): IActionSetError => ({
    type: ACTION_TYPE.setError,
    payload
  }),
  removeError: (payload: UUID): IActionRemoveError => ({
    type: ACTION_TYPE.removeError,
    payload
  })
}
