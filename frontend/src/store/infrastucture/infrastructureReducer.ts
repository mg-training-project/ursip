import {
  IActionInfrastructure,
  IStateInfrastructure,
} from './IStateInfrastructure.interface'
import {ACTION_TYPE} from '../ActionType.enum'
import {DEFAULT_STATE_INFRASTRUCTURE} from './defaultStateInfrastructure.const'
import {AppErrorFactory} from '../../Infrastucture/AppError/AppError.factory'

export const infrastructureReducer = (
  state: IStateInfrastructure = DEFAULT_STATE_INFRASTRUCTURE,
  action: IActionInfrastructure,
) => {
  const errorFactory = new AppErrorFactory()

  switch(action.type) {
    case ACTION_TYPE.loading:
      return {
        ...state,
        loading: action.payload,
      }
    case ACTION_TYPE.setError:
      return {
        ...state,
        errorList: [...state.errorList, errorFactory.create(action.payload)],
      }
    case ACTION_TYPE.removeError:
      return {
        ...state,
        errorList: [...state.errorList.filter(
          error => error.id !== action.payload
        )],
      }
    default:
      return state
  }
}
