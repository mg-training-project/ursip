import {IStateArticle} from './article/IStateArticle.interface'
import {IStateComment} from './comment/IStateComment.interface'
import {
  Page,
  PageSize,
} from '../Infrastucture/Pagination/Pagination.types'
import {
  IStateInfrastructure
} from './infrastucture/IStateInfrastructure.interface'

export interface IAppState {
  article: IStateArticle
  comment: IStateComment
  infrastructure: IStateInfrastructure
}

export interface IPageListState {
  currentPage: Page
  pageSize: PageSize
  list: any[]
  searchString: string
}
