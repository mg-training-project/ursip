import {applyMiddleware, combineReducers, createStore} from 'redux'
import {articleReducer} from './article/articleReducer'
import {commentReducer} from './comment/commentReducer'
import {IAppState} from './AppState.interface'
import thunk from 'redux-thunk'
import {infrastructureReducer} from './infrastucture/infrastructureReducer'

const rootReducer = combineReducers<IAppState>({
  article: articleReducer,
  comment: commentReducer,
  infrastructure: infrastructureReducer,
})

export const store = createStore(rootReducer, applyMiddleware(thunk))
