import {actionsArticle} from './article/actionsArticle'
import {actionsComment} from './comment/actionsComment'
import {actionsInfrastructure} from './infrastucture/actionsInfrastructure'

export const actions = {
  article: actionsArticle,
  comment: actionsComment,
  infrastructure: actionsInfrastructure
}
